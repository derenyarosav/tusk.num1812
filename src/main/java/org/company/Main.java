package org.company;

import org.company.details.Engine;
import org.company.entities.Person;
import org.company.professions.Driver;
import org.company.vehicles.entities.Car;
import org.company.vehicles.entities.Lorry;
import org.company.vehicles.entities.SportCar;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Toyota", "X4", 1100,
                new Driver(5, "Olexandrovich", "Alex", "Prokopenko",
                        23, "Male", 0672314231),
                new Engine("239 к.с. (178 кВт) при 5200 об/хв", "Camigo Plant"));
        SportCar sportCar = new SportCar("Ferrari", "Sport Car", 500,
                "300 км/год", new Driver(7, "Leonidovich", "Oleg"
                , "Norenko", 54, "Male", 0673213432), new Engine("239 к.с. (178 кВт) при 5200 об/хв",
                "Ferrari"));
        Lorry lorry = new Lorry("Kamaz", "Lorry", 5000, 10000,
                new Driver(5, "Olexandrovich", "Alex", "Prokopenko",
                        23, "Male", 0672314231), new Engine("239 к.с. (178 кВт) при 5200 об/хв",
                "Ferrari"));
        System.out.println(car.toString());
        System.out.println(lorry.toString());
        System.out.println(sportCar.toString());
        car.start();
        car.stop();
        car.turnRight();
        car.turnLeft();
        sportCar.start();
        lorry.start();

    }
}