package org.company.vehicles.entities;

import org.company.details.Engine;
import org.company.professions.Driver;

public class SportCar extends Car {
    private String maximumSpeed;

    public SportCar(String carBrand, String carMark, int wight, String maximumSpeedDriver, Driver driverLicense,
                    Engine motor) {
        super(carBrand, carMark, wight, driverLicense, motor );
        this.maximumSpeed = maximumSpeed;

    }

    public String getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(String maximumSpeed) {
        maximumSpeed = maximumSpeed;
    }

    @Override
    public void start() {
        System.out.println("Починаю швидко їхати ");


}

    @Override
    public String toString() {
        return "SportCar{" +
                "maximumSpeed='" + maximumSpeed + '\'' +
                '}';
    }
}
