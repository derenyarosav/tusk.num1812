package org.company.vehicles.entities;

import org.company.details.Engine;
import org.company.professions.Driver;

public class Car {
    private String carBrand;
    private String carMark;
    private int wight;

    private Driver driverLicense;

    private Engine motor;

    public Car(String carBrand, String carMark, int wight, Driver driverLicense, Engine motor) {
        this.carBrand = carBrand;
        this.carMark = carMark;
        this.wight = wight;
        this.motor = motor;
        this.driverLicense = driverLicense;
    }

    public Engine getMotor() {
        return motor;
    }

    public void setMotor(Engine motor) {
        this.motor = motor;
    }

    public Driver getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(Driver driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarMark() {
        return carMark;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Start");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");

    }

    @Override
    public String toString() {
        return "Car{" +
                "carBrand='" + carBrand + '\'' +
                ", carMark='" + carMark + '\'' +
                ", wight=" + wight +
                ", driverLicense=" + driverLicense +
                ", motor=" + motor +
                '}';
    }
}


