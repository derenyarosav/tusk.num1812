package org.company.vehicles.entities;

import org.company.details.Engine;
import org.company.professions.Driver;

public class Lorry extends Car {


    private int carryingCapacityOfTheBody;

    public Lorry(String carBrand, String carMark, int wight, int carryingCapacityOfTheBody, Driver driverLicense,
                 Engine motor) {
        super(carBrand, carMark, wight, driverLicense,motor);
        this.carryingCapacityOfTheBody = carryingCapacityOfTheBody;

    }


    public int getCarryingCapacityOfTheBody() {
        return carryingCapacityOfTheBody;
    }


    public void setCarryingCapacityOfTheBody(int carryingCapacityOfTheBody) {
        carryingCapacityOfTheBody = carryingCapacityOfTheBody;
    }

    @Override
    public void start() {
        System.out.println("Починаю таягнути вантаж");
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "carryingCapacityOfTheBody=" + carryingCapacityOfTheBody +
                '}';
    }
}

