package org.company.professions;


import org.company.entities.Person;

public class Driver extends Person {
    private int drivingExperience;

  public Driver(int drivingExperience,String patronymic,String name,String surname,
                int age,String gender,int phoneNum){
      super(name,surname ,patronymic,age,gender,phoneNum);

      this.drivingExperience = drivingExperience;
  }

    public int getdrivingExperience() {
        return drivingExperience;
    }

    public void setdrivingExperience(int drivingExperience) {
        drivingExperience = drivingExperience;

    }

    @Override
    public String toString() {
        return "Driver{" +
                "drivingExperience=" + drivingExperience +
                '}';
    }
}
